//Celine Tran 1938648
package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {
	
	

	@Test //this will test the getSpeed() and getLocation() method
	public void testGetMethods() {
		Car testCarSpeed= new Car(5);
		int expectedSpeed=5;
		int actualSpeed = testCarSpeed.getSpeed();
	
		int expectedLocation=0;
		int actualLocation = testCarSpeed.getLocation();
		
		 assertEquals(expectedSpeed,actualSpeed);
		 assertEquals(expectedLocation,actualLocation);

	}
	
	@Test//Tests the accelerate method with positive speed
	public void testAccelerateMethod() {
		Car testCarAccelerate= new Car(9);
		int expectedSpeed = 10;
		testCarAccelerate.accelerate();
		int actualSpeed = testCarAccelerate.getSpeed();
		
		assertEquals(expectedSpeed,actualSpeed);
	}
	@Test//Tests the method decelerate. Should decrease by 1 in speed.
	public void testDecelerateMethod() {
		Car testCarDecelerate1= new Car(3);
		int expectedSpeed = 2;
		testCarDecelerate1.decelerate();
		int actualSpeed = testCarDecelerate1.getSpeed();
		
		assertEquals(expectedSpeed,actualSpeed);
	}
	
	
	@Test //Tests the speed when it is already at zero. Should not decrease further.
	public void testDecelerateMethodZero() {
		Car testCarDecelerate2= new Car(0);
		int expectedSpeed = 0;
		testCarDecelerate2.decelerate();
		int actualSpeed = testCarDecelerate2.getSpeed();
		
		assertEquals(expectedSpeed,actualSpeed);
	}
	
	
	
	@Test//tests the moveright() method with positive speed
	public void testMoveRightMethod() {
	Car testCarLocation1 = new Car (4);
	int expectedLocation = 4;
	testCarLocation1.moveRight();
	int actualLocation = testCarLocation1.getLocation();
	
	assertEquals(expectedLocation,actualLocation);
	}
	
	@Test//tests the moveleft() method with a positive speed. should start at zero and decrease
	public void testMoveLeftMethod() {
		Car testCarLocation2 = new Car (6);
		int expectedLocation = -6;
		testCarLocation2.moveLeft();
		int actualLocation = testCarLocation2.getLocation();
		
		assertEquals(expectedLocation,actualLocation);
	}
	
	@Test// tests the constructor Car when the speed is negative
	public void TestConstructorNegative() {
	    try {
	        Car testCarSpeed2 = new Car(-5); // should throw an exception and throw error
	        
	            fail("The speed should not be negative. It should have thrown");  
	        }
	        catch (IllegalArgumentException e) {      
	        }
	}
	
	@Test// tests the constructor Car when the speed is positive
	public void TestConstructorpositive() {
	        Car testCarSpeed2 = new Car(5); 
	     int expectedConstuctorSpeed = 5;
	     int actualConstructorSpeed = testCarSpeed2.getSpeed();
	     
	     assertEquals(expectedConstuctorSpeed,actualConstructorSpeed);
	}
	
}
