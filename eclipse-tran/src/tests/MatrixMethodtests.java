//Celine Tran 1938648
package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodtests {

	@Test// test the duplicate method to see if the column array will duplicate
	public void testMethodDuplicate() {
		
	int[][] arr = {
            {1,2,3},
			{4,5,6}
		};
	int [][] expectedArr = {
			{1,2,3,1,2,3},
			{4,5,6,4,5,6}
	};
	int [][] actualArr = MatrixMethod.duplicate(arr);
	
	assertArrayEquals(expectedArr,actualArr);
	
}
}
	




